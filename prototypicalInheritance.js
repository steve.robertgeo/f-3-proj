let obj1 = {
  nam: "Aditya",
  mobileNumber: "100",
};

let obj2 = {
  nam: "anurag",
  city: "delhi",
};

console.log(obj1.city);

// obj1.__proto__ was Object.prototype
// now it's obj2
obj1.__proto__ = obj2;

console.log(obj1.city);
