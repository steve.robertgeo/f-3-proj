let obj={
    nam: 'anurag',
}

function intro(city,company){
    console.log(this);
    console.log('hi my name is '+ this.nam + ' I live in '+city+ ' I work at ' + company);
}

// intro.call(obj,'delhi','R&AW');
// intro.call(obj,'noida','cars24');
// intro.call(obj,'chennai','zoho');
// intro.call(obj,'blr','flipkart');
// intro.call(obj,'lahore','isis');

// intro.call(obj - this thing needs to be wrapped 

console.log('-------');
const intro2 = intro.bind();

// intro2 is following defi of intro
// and value of this keyword is obj

let obj2 = {
    nam:'rizwan',
}

intro2.call(obj2,'delhi','R&AW');
// intro2('noida','cars24');
// intro2('chennai','zoho');

