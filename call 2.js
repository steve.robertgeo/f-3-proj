let obj1 = {
  nam: "ravi",
  city: "patna",
  intro() {
    console.log(this);
  },
};

let obj2 = {
  nam: "prakriti",
  city: "ajmer",
};

// implicit -> naturally value of this keyword
// explicit -> forcefully udating the value of this keyword

// this is implicit binding
obj1.intro(); // value of this key word is obj1

// this is explicit binding
obj1.intro.call(obj2); // value of this keyword is obj2

obj1.intro.call(obj1); // inception happened
