let obj = {
  nam: "azaz",
};

// function intro(city, company) {
//   console.log(
//     "hi my name is " + this.nam + " I live in " + city + " I work at " + company
//   );
// }
function intro(company, city) {
  console.log(
    "hi my name is " + this.nam + " I live in " + city + " I work at " + company
  );
}

// intro.call(obj,'blr','walmart');
// intro.call(obj,'blr','flipkart');
// intro.call(obj,'blr','cisco');
// intro.call(obj,'blr','cred');

// earlier we saw this keyword fixed to obj
// now i want a argumnet to be fixed

const intro2 = intro.bind(obj,'cred');
  
intro2('delhi','cred');
intro2('flipkart');

function intro2(company) {
    city = 'blr'
    console.log(
      "hi my name is " + this.nam + " I live in " + city + " I work at " + company
    );
  }

// if you are passing arguments in .bind then you are fixing those parameters
// for newFn

// curry
// fn(1,2) -> fn(1)(2)
