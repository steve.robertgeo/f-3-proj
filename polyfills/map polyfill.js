// prototype is a key in Array object
// that is storing map


// writing code of any function that
// is generated by browser is known as polyfill
// why we do this? 
// to make our project compatible for old browsers

// delete Array.prototype.map;


Array.prototype.map = function(callback){
    const res = []; 
    // this will be pointing to array
    for(let i=0;i<this.length;i++){
        // this[i] -> current element -> arr[i]
        // i -> index
        // this -> arr
        res[i]=callback(this[i],i,this);
    }
    return res;
}
// let arr = [10, 5, 30];

// arr.map((item,index,arr)=>{
//     console.log(item,index,arr);
// })
let final = [10, 5, 30].map((item) => item * 10);
console.log(final);


// user can be current element
// index and whole in map arugments


// 10 -> callback(10); 
// 5 -> callback(5); 
// 30 -> callback(30);


// Array.prototype.test = function(){
//     console.log(this);
// }

// let arr = [1,2,3];
// let arr2 = ['anurag','aditya'];
// arr2.test();
// arr.test();


