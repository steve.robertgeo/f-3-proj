function fn1(a, b, c) {
  //   console.log(this);
  //   console.log(a, b, c);
}

let obj = {
  a: 10,
};

Function.prototype.bind = function (object, ...rest1) {
  const oldFn = this;
  return function (...rest2) {
    return oldFn.call(object, ...rest1, ...rest2);
  };
};

const fn2 = fn1.bind(obj, 1, 2);

fn2(3, 4);
const arr1 = [1, 2];
const arr2 = [3, 4];

fn1.call(obj, ...arr1, ...arr2);

// fn2() -> fn.call();
// fn2 = function(){
    // return fn.call();
// }
// bind = function(){
    // returns function(){
    // return fn.call();
// }
// }