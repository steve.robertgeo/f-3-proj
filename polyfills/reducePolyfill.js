// let arr =      [1,2,3];
// sumSoFar ->1,3,6

// callback -> *,/,+,-

// let sum = arr.reduce((accumulator,currEle,index,arr)=>{
//     // accumulator -> calculateed value so far
//     // this callback fn will return computed value of accumulator so far
//     // and it will also update the
//     console.log(`accu for ${index} index-->`,accumulator);
//     console.log(`curr for ${index} index-->`,currEle);
//     return accumulator + currEle;
// },0);

// sum final be having value of accumulator
// let initialValue = 0;
// let acc = initialValue;
// for(let i=0;i<3;i++){
//     acc = acc+ arr[i];
// }

// console.log(acc);

Array.prototype.reduce = function (callback, initialValue) {
  let accumulator = initialValue; // this is undefined
  // if initialValue is undefined
  // we take the value of arr[0]
  // accumulator = initialValue ? initialValue : this[0];
  for (let i = 0; i < this.length; i++) {
    // this if condition will be executed only once when accumulator is undefined
    // this will run only once
    if (accumulator === undefined) {
      accumulator = this[i];
    }
    else{
        accumulator = callback(accumulator, this[i], i, this);
    }
  }
  return accumulator;
};

let arr = [2, 2, 3];

let multi = arr.reduce((acc, cur) => {
  return acc * cur;
});

console.log(multi);

// function sum(a,b){
//     return a+b;
// }

// let a = 10;

// a = sum(a,5);

// a = a+5;
