// here they are rest operator
// Function.prototype.call = function (object, ...restParam) {
//     // this keyword stores function defition
//     object.tempFn = this; // here I stored fn defi in tempFn
//     const result = object.tempFn(...restParam); // here i calling tempFn with object implicitly
//     // here these '...' will be spread operator
//     delete object.tempFn; // we need to delete that tempFn
//     return result;
// };

// fn.call() here fn is acting as an object
// and so this keyword will point to fn defition

let obj = {a:10};

console.log(obj);

function sum(a,b,c){
    console.log(this.a);
    return a+b+c;
}

sum.call(obj,10,20,30);
console.log(obj);


// call = [1,2,3];
// fn(1,2,3)



