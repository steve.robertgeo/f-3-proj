function test(a, b, ...aditya) {
  console.log(a, b, aditya);
}

// restParam converts all the arguments
// into an array

// '...' can be spread as well as
// rest param

// restParam combines the parameters
// in function defition into an array

// spread spreads the given thing
// it can be array as well as object

test(1, 2, 3, 4);

// '...' inside functional defi
// will be rest operator
// anywhere else it will spread operator
