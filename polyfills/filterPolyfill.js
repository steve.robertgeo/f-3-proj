// arr = [ele1,ele2,ele3]

// res = []

// callback(ele1) -> if this is true -> it will be pushed inside res
// else it won't be pushed inside res

delete Array.prototype.filter;

Array.prototype.filter = function(callback){
    // callback will either give me true or false
    const res = []; // clear?
    for(let i=0;i<this.length;i++){
        // callback will give me true or false for this[i]
        if (callback(this[i], i, this) === true) {
          res.push(this[i]);
        }
    }
    return res;
}

let peopleAge = [19, 9, 29, 17];

let votersList = peopleAge.filter(function(item,index,peopleAge){
    return item >=18;
});

console.log(votersList);
