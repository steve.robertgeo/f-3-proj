Function.prototype.apply = function(obj,array){
    obj.tempFn = this; // this is function
    const result = obj.tempFn(...array);
    delete obj.tempFn;
    return result;
}


// fn.apply
// this keyword is pointing towards
// fn