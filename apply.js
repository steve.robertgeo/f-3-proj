// // call and apply they both are exactly same
// let obj={
//     nam: 'sanket',
// }

// function sum(a,b) {
//   console.log(this);
//   console.log(a,b);
// }

// // case 1 if there are no arguments -> no difference
// // sum.call(obj);

// // sum.apply(obj);

// // case 2 if there are arguments

// let arr = [1,2,3,4,5,6,7,8,8,9];

// sum.call(obj,[1,2]);

// sum.apply(obj,arr);


// normal call -> if a function has argumnets alpha, beta, gamma, and so....
// .call       -> here we will call it with obj, alpha, beta, gamma and so......
// .apply      -> here we will call it with obj, [alpha,beta,gama and so.....]

// total number of argumnets apply can accept is only 2
// obj, []

// function myFunction(a, b, c) {
//     console.log(a, b, c);
// }

// // Using call() to pass arguments as an array
// myFunction.call(null, ...[1, 2, 3]);  // Output: 1 2 3

// console.log(...[1, 2, 3])
