// 3rd class

class User{
    // constructor is keyword
    constructor(name,bank,balance){
        this.name = name;
        this.bank = bank;
        this.balance = balance;
    }
    withdraw(amount){
        this.balance = this.balance - amount;
    }
}
// class is syntactical sugar over constructor function

// syntax wise
// class => function + protoype object

const user10 = new User('rajat dalal','sbi',4500);
console.log(user10);
user10.withdraw(500);