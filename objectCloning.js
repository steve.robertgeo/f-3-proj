// 1st method -> assignment operator (shallow clone)

// let obj1 = {
//     a:10,
// }

// let obj2 = obj1;

// console.log({ a: 10 } === { a: 10 });

// console.log('obj1--->',obj1);
// console.log('obj2--->',obj2);

// obj2.a = 100;

// console.log('obj1--->',obj1);
// console.log('obj2--->',obj2);

// 2nd method -> spread operator (...) -> shallow clone

// let obj1 = {
//   a: 10,
// };

// let obj2 = { ...obj1 };

// console.log('obj1--->',obj1);
// console.log('obj2--->',obj2);

// obj2.a = 100;

// console.log('obj1--->',obj1);
// console.log('obj2--->',obj2);

// let obj1 = {
//   a: 10,
//   b: {
//     c: 100,
//   },
// };

// let obj2 = { ...obj1 };

// console.log('obj1--->',obj1.b);
// console.log('obj2--->',obj2.b);

// obj2.b.c = 'c value';
// console.log("----changes occured----");

// console.log('obj1--->',obj1.b);
// console.log('obj2--->',obj2.b);


// let obj1 = {
//   a: 100,
//   b: {
//     c: 100,
//     d: {
//       e: 300,
//     },
//   },
// };
    

// let obj2= {...obj1};

// console.log('obj1--->',obj1.b);
// console.log('obj2--->',obj2.b);

// obj2.b.c = 'c value';
// console.log("----changes occured----");

// console.log('obj1--->',obj1.b);
// console.log('obj2--->',obj2.b);

// JSON.stringify and JSON.parse

// let obj1 = {
//   a: 10,
// };

// // JSON.stringify converts object intro string form
// let objStr = JSON.stringify(obj1);

// console.log(obj1);
// console.log(objStr);

let obj1 = {
  a: 10,
  b: {
    c: 100,
  },
};

let obj2Str= JSON.stringify(obj1);
let obj2 = JSON.parse(obj2Str);

console.log('obj1--->',obj1.b);
console.log('obj2--->',obj2.b);

obj2.b.c = 95;
console.log("----changes occured----");

console.log('obj1--->',obj1.b);
console.log('obj2--->',obj2.b);

