// function test() {
//   console.log(this);
// }

// test();

// var a = 10;

// console.log(window.a);

let obj = {
  firstName: "salik",
  lastName: "arora",
  intro() {
    console.log(this);
  },
};

// implicit binding
intro(); /// this keyword is window

// explicit bindind
intro.call(obj); // this keyword is obj

// my intro function is binded with window object
