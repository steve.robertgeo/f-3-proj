# Instructor
- [linkedin](https://www.linkedin.com/in/prikshit8/)

# Class 1 - 7th March Thursday
- deep clone - if we clone any object without any ref
                then it's known as deep clone

- shallow clone - if we clone any object and it gets any ref
                    then it's known as shallow clone

- shallow clone methods ->
<ol>
<li>assignment operator</li>
<li>spread operator</li>
</ol>

- deep clone methods ->
<ol>
<li>json parse json stringify</li>
</ol>

- [notes](https://drive.google.com/file/d/1l-engIXJnX5cOZlL36x5fsaxt4kVdB9E/view?usp=sharing)

# Class 2 - 8th March Friday
- [notes](https://drive.google.com/file/d/11IkyL8UnlqN_Fgd0SqYPjyX2APe3FYbP/view?usp=sharing)


# Class 3 - 11th March Monday
- [notes](https://drive.google.com/file/d/1npKlJnM4S9D5Shz1dShqn1qKh7kIQy_k/view?usp=sharing)


# Class 4 - 12th March Tuesday
- [notes](https://drive.google.com/file/d/1tZHV0ulv_raM-JEaP3Ep9o2NQgqI7VIN/view?usp=sharing)

# Class 5 - 13th March Wednesday
- [notes](https://drive.google.com/file/d/1o20cW5KH0LEhRAH-AV_PBqQBGIBxblgW/view?usp=sharing)

# Class 6 - 14th March Wednesday
- [notes](https://drive.google.com/file/d/1M45Inuyu5ZzU2p4d89RJNnC6JuxxMFp_/view?usp=sharing)