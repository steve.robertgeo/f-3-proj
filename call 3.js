// function sum(a, b) {
//   console.log(this);
//   return a + b;
// }

let obj = {
  nam: "anurag",
};

// console.log(sum(1, 2));

// console.log(sum.call(obj, 10, 20));

function test(a, b, c) {
  console.log(a, b, c, this);
}

test(1,2,3);

test.call(obj,10,20,30);

// if you are passing arguments alpha, beta, gamma, so ...... to a function fn
// then you need to pass object, alpha, beta, gamma, so ...... to fn.call

// call method can take any number number arguments
// if fn accepts n arguments
// fn.call will accept n+1 arguments